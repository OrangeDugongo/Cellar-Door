---
title: F is for Fantastic
author: Raffaele
date: 2016-01-19
categories:
    - serie-tv
    - netflix
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*zILfNMgQWMi0NXjHR1mb5A.jpeg
permalink: /serie-tv/f-is-for-family/
---

Nella vita non mi sono mai piaciti i giri di parole, ho sempre preferito la chiarezza, l'essere diretto, quindi anche questa volta andrò dritto al punto; F is for Family è una buona serie, mi ha divertito, mi è piaciuta, ma non è ai livelli di [_BoJack Horseman_][1]. Indubbiamente _F is for Family_ è la migliore seria animata su una famiglia. Dimenticatevi la demenzialità dei Griffin o le assurde avventure dei Simpson, la serie Netflix cerca di restare quanto più possibile ancorata alla realtà. I personaggi sono fortemente stereotipati; c'è il padre frustato dal lavoro, la madre stanca della sua condizione di casalinga, il figlio sfogliato che sputa su tutto ciò che la famiglia gli offre, e così via. Tutto questo potrebbe fa storcere il naso a qualcuno, ma gli stereotipi sono funzionali alla trama, non solo permettono allo spettatore di rispecchiarsi facilmente in almeno un personaggio ma anche di avere un quadro preciso della situazione. Gli stereotipi sono una base, uno schizzo a cui, nel corso delle puntate(sfortunatamente solo sei 😔), vengono aggiunti sempre nuovi dettagli e sfaccettature. In questo senso i due autori hanno svolto un fantastico lavoro di scrittura, un uso così saggio e accurato degli stereotipi e raro. L'intera serie(caratterizzata da una trama orizzontale) si regge sul debole equilibrio familiare dei Murphy e lo fa con eleganza, senza esagerazioni o forzature di trama. Forte è anche la componente critica della serie. Lo spettatore è portato sempre a condannare sia il comportamento sia i rapporti che si instaurano tra i vari personaggi, ma anche in un certo senso ad assolverli, non per buonismo, ma perché ne comprende le ragioni e la frustrazione. Tutto ciò da origine ad una serie malinconica e dal sapore agrodolce. Si certo si ride, ma sono quasi sempre risate amare.

Volendo trovare il pelo nell'uovo bisogna dire che nella serie ci sono molte scene esplicite e volgari, a volte giustificate dalla situazione, altre volte gratuite e fini a se stesse. Questo non è necessariamente un difetto, ma potrebbe infastidire alcuni spettatori. Inoltre anche la comicità in _F is for Family_&nbsp;è leggermente più stereotipata rispetto BoJack, le battute sono meno ricercate, spesso si preferisce la strada più semplice e qualche volta diventano anche ripetitive, ma niente di così grave da pregiudicare la qualità generale della serie.

[1]: /serie-tv/bojack-horseman-storia-un-rinunciatario/
