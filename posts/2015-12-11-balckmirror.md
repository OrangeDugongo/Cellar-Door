---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*dfLgphKWe3CCSaYO1PNpKQ.jpeg
title:  "Black Mirror  - White Christmas"
date:   2015-12-11
author: Raffaele
categories:
    - serie-tv
    - netflix
permalink: /serie-tv/black-mirror-white-christmas/
---

Non sono mai stato un tipo ordinato, ma neanche un disordinato che si trova nel suo disordine. Io sono un disordinato DOC, uno di quelli che non trova mai niente. Ma nonostante questo non ho mai pensato di cambiare, vuoi per pigrizia, vuoi perché per quanto mi posso impegnare l'universo tenderà comunque all'entropia, eppure nella mia vita ho avuto sparuti incostanti sprazzi d'ordine.

_Non vedo cosa possa centrare il tuo ordine con i consigli di oggi._

Caro lettore se mi dai il tempo di finire ti sarà tutto più chiaro. In un breve momento d'ordine ho provato a categorizzate _Black Mirror_, ma con scarsi risultati. Per Wikipedia è una serie TV britannica; e in effetti è così, ma è diversa da qualsiasi altra serie TV, infatti ogni puntata è una storia a se.

_Ho capito è una serie TV antologica, come True Detective e American Horror Story._

Ni, le serie che hai citato cambiano trama ogni stagione, mentre _Black Mirror_ ha storie e personaggi diversi per ogni episodio, quarantacinque minuti per conoscere il protagonista, affezionarsi a lui e dirgli addio. Ti sembrano pochi? Si lo sono, ma se hai creatività e qualcosa da dire puoi fare grandi cose in quarantacinque minuti. Comunque oggi non parleremo di_Black Mirror_.

_Mind blown&nbsp;😵._

Non so bene come definirci, ma sicuramente siamo strani. Preferiamo apparire più tosto che essere, siamo disposti a tradire la fiducia di chi ci sta intorno, non ci facciamo scrupoli ad affossare chi sta peggio di noi pur di perseguire il nostro scopo. A volte siamo pronti perfino a schiavizzare noi stessi. Compriamo cose che non ci servono, con soldi che non abbiamo, per impressionare persone che non ci piacciono. Crediamo che la tecnologia ci salverà, ma non ci rendiamo conto che ci sta facendo più male che bene. Abbiamo dimenticato cosa vuol dire fare due chiacchiere, non sappiamo più confrontarci. Risolviamo i problemi zittendo le persone, le blocchiamo sui social network e impariamo quant'è brutto essere soli quando ormai è troppo tardi.

Tutto questo cosa c'entra? C'entra che gli inglesi vanno pazzi per gli speciali natalizi. C'entra che tutte queste cose che ti ho appena raccontato le troverai in _White Christmas_, l'episodio speciale da un ora e mezza di _Black Mirror_, approdato da poco su Netflix. C'entra che non mi lascerei scappare quest'occasione, soprattutto ora che siamo sotto Natale 🎄.  
