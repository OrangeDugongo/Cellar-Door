---
title: BoJack Horseman, storia di un rinunciatario
author: Raffaele
date: 2015-12-29
categories:
    - serie-tv
    - netflix
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*h0nSfBLk8ypnCEGu_6Wktg.jpeg
permalink: /serie-tv/bojack-horseman-storia-un-rinunciatario/
---

Non sono mai stato un gran fan delle comedy (nel mio cuore c'è solo il drama 😍), ma nonostante tutto nella mia carriera da telespettatore ho iniziato numerose sit-com che a primo acchito mi piacevano, anche molto, ma dopo un paio di episodi smettevano di farmi ridere. Non lo so, avrò qualche problema io, ma le uniche comedy che hanno avuto vita lunga sono New Girl e le serie animate della Fox tra tutte I Simpson e Family Guy. Ed è proprio nella speranza di trovare il nuovo Simpson made in Netflix che ho iniziato _BoJack Horsman_.

La prima impressione non è stata delle migliori, animali antropomorfi al posto degli omini gialli, non è certo il massimo della creatività. Una parte di me dopo qualche minuto voleva chiudere tutto e andare a dormire, poi così, senza ben capire come, avevo finito la prima stagione. Vittima del binge watching. Mi sbagliavo, mi sbagliavo di grosso. _BoJack Horsman_ non ha nulla a che vedere con Homer e famiglia, BoJack è di tutt'altro pianeta. Bastano pochi minuti per accorgersi delle profonde differenze tra le due serie. Solitamente le comedy hanno una trama per lo più verticale, ogni episodio è a se stante e auto conclusivo. Puoi vedere gli episodi in ordine sparso senza avere grossi problemi. Con la serie Netflix la situazione è ben diversa, la trama è orizzontale, ogni puntata è strettamente collegata con la precedente. Alla comicità, basata prevalentemente sul no sense e sul ridicolo, Bojack risponde con una comicità più adulta e ricercata. Le battute non sono finalizzate solo a far ridere lo spettatore, ma anche a farlo riflette sul [sentimento del contrario][1]. Anche i personaggi sono diversi, di solito nelle commedie si prediligono personaggi stereotipati ed è proprio intorno a questi stereotipi che ruotano la maggior parte delle gag, invece BoJack, come i suoi coprimari, è caratterizzato benissimo. Nel corso delle puntate lo conosceremo fin nel profondo, sapremo tutto di lui, una cura così minuziosa è veramente cosa rara, soprattutto in una comedy.

_Di cosa parla BoJack Horseman?_

Dare una definizione precisa di un'opera così sfaccetta non solo sarebbe difficile, ma anche riduttivo, per questo quando sono costretto a farlo me ne esco con un: «&nbsp;_BoJack Horseman_ è la versione animata di [_Birdman][2]&nbsp;_senza piano sequenza.&nbsp;»

_Horsin' Around_, serie TV anni '90, ha reso BoJack famoso presso il grande pubblico, ma si sa la fama è fugace, così una volta finito lo Show il nostro protagonista è finito nel dimenticatoio, trascorrendo gli ultimi venticinque anni a sperperare i suoi soldi in droga, alcol e zucchero filato. Messo difronte alla sua carriera fallimentare decide di scrivere un libro per raccontare il vero se. libro che è un pretesto narrativo per mostraci un modo dello spettacolo malato, popolato da persone superficiali e vuote. Una critica cinica e forte che assume ancora più potenza perché emessa dallo stesso show business. BoJack incarna alla perfezione la figura del fallito. Qualsisia lavoro gli venga affidato in un modo o in un altro andrà a finire male e lui lo sa per questo ormai non ci prova neanche più. La Serie Netflix non si limita a mostraci il nostro attore preferito solo sul palcoscenico, ma anche nella vita privata, ed è proprio in questo punto che l'equilibrio della serie si sposta sul dramma e ci regala sorrisi amari. BoJack, nel corso della sua vita, non è mai stato capace di instaurare un rapporto di qualsiasi tipo. Non è mai stato un figlio, non ha mai avuto amici e non ha mai avuto una storia d'amore. Potrebbe sembrare un perfetto stronzo, ma la sua cattiveria è innocente. Lui è un adulto mai cresciuto, un bambino che in una calda giornata estiva si diverte ad abbrustolire le formiche con la lente di ingrandimento, non lo fa per cattiveria, ma perché ignora quello che sta facendo. La forza di _BoJack Horseman_ risiede proprio nel suo protagonista che incarna il qual senso inadeguatezza ed estraneità che proviamo.

Inquesti ultimi giorni di dicembre siamo tutti pronti a fare bilanci e classifiche ed io non voglio essere da meno. In questo mio 2015 dopo due mostri sacri come [Fargo][3] e [The Leftovers][4] viene, senza ombra di dubbio, questo piccolo capolavoro d’animazione.



[1]: http://www.oilproject.org/lezione/riassunto-luigi-pirandello-poetica-umorismo-6525.html
[2]: /cinema/birdman/
