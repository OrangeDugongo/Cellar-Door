---
title: Memento, la costante ricerca di un Ubi Consistam
author: Raffaele
date: 2016-03-04
categories:
    - cinema
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*SlBa3bdqGqBjHC6Z6gJpGQ.jpeg
permalink: /cinema/memento-la-costante-ricerca-un-ubi-consistam/
---

Caro lettore, ti avevo già parlato in passato di _Christopher Nolan_ in un&nbsp;[lunghissimo post][1] in cui analizzavo il suo stile cinematografico, bene, sono convinto di non averti annoiato ancora abbastanza, per cui oggi andrò più nello specifico e ti parlerò di uno dei suoi lavori che ho apprezzatore di più; Memento. Suo secondo lungometraggio dopo _Following_, basato sull'omonimo racconto del fratello Jonathan, segue la storia di Leonard affetto, in seguito ad un incidente, da un grave disturbo della memoria a breve termine. Questa insolita patologia del protagonista permette a Nolan di dar sfogo a tutta la sua creatività e di creare un'insolita struttura non lineare che diventerà un vero e proprio marchio di fabbrica del regista. Per fare ciò il regista si affida ad un montaggio alquanto insolito. Allo spettatore viene mostrata prima l'ultima scena a livello temporale, poi la prima, poi la penultima e così via finché il film non si conclude con le scene centrali. Questa scelta stilistica consente al regista di tenere sempre alta l'attenzione dello spettatore che si ritrova nella stessa situazione mnemonica del protagonista.

Laregia è buona, ma non eccelsa; molto tecnica e operaia anche se in qualche punto si riesce a cogliere qualche guizzo e le inquadrature diventano più stilose e ricercate. Naturalmente il film è a basso budget quindi niente affettoni come in _Inception_ o _Interstellar_, qui è tutto al naturale. Diciamo che dal punto di vista visivo il film non eccelle più di tanto. la sceneggiatura, scritta a quattro mani dai fratelli Nolan, per quanto originale e interessante non riesce a portare il film su un altro livello. A fare realmente la differenza in questa produzione è la messa in scena e l'impostazione che il regista riesce a conferire alla sua creatura. L'intera storia poggia sul concetto di narratore inaffidabile; Lo spettatore non sa mai se credere o meno al narratore. In alcuni casi resta sul vago, preferendo narrare i fatti in modo sfumato, mentre altre volte fornisce più versioni dello stesso evento([vi ricorda qualcosa?][2]). Lo spettatore è alla costante ricerca di un punto fermo, cerca in tutti i modi di dare una direzione alla storia, ma non riesce mai a far combaciare tutti i pezzi del puzzle.

>Non ci sono fatti, ma solo interpretazioni.  
>Friedrich Nietzsche

Il regista si prende gioco della natura umana; ad ogni cambio di inquadratura ogni convinzione dello spettatore viene distrutta, ma immediatamente dentro di lui scatta anche quel meccanismo inconscio che lo porta a creare nuove ipotesi, nuove teorie destinate a cadere inesorabilmente nel giro di pochi minuti. Solo dopo le due ore di proiezione è possibile tirare qualche somma, dare un interpretazione al tutto, ma niente di più. Lo spettatore ne esce scosso, infastidito, disorientato, non conosce realmente i fatti, ma alla fine va bene così. A lui, a noi, basta credere in qualcosa, poi se questo qualcosa sia vero o meno non conta, l'importante è credere.

[1]: /cinema/apologia-christopher-nolan/
[2]: http://www.bestmovie.it/news/le-cicatrici-di-joker-la-stanza-237-di-shining-e-altri-misteri-irrisolti-della-storia-del-cinema-scoprili-nella-nostra-gallery/264752/nggallery/image/il-cavaliere-oscuro-da-dove-arrivano-le-cicatrici-del-joker/
