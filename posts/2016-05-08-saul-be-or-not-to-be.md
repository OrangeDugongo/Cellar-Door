---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*h2TPSodvfV_piSSDATXAqw.jpeg
title:  "Saul, Be or not to Be"
#subtitle: "Feel home!"
date:   2016-05-08
author: Raffaele
categories:
    - serie-tv
permalink: /serie-tv/better-call-saul-be-or-not-to-be/
---

Sarò sincero, la seconda stagione di **_Better Call Saul_** non è stata come l'avrei immaginata. Ero convinto che in questa seconda tranche di episodi avremmo visto il Saul a cui tutti abbiamo imparato a voler bene. Ero convinto che dopo una [prima stagione][1] decisamente più lenta della serie madre, gli autori avrebbero spinto con più decisione il perde sull'acceleratore e invece no. Ce ne ho messo di tempo, ma **F**inalmente, dopo venti puntate, mi è ormai chiaro che _Better Call Saul_ non è _Breaking Bad_. Non lo è ora e con grande probabilità non lo sarà mai, ma cosa ancora più importate, _Better Call Saul_ non vuole essere _Breaking Bad_.

Lo show in questi due anni è cresciuto diventando grande, lasciandosi alle proprio spalle il passato. I**&nbsp;R**iferimenti alla serie madre sono solo un di più che non solo contribuisce a dare un sussulto agli spettatori di vecchia data, ma permette anche di creare un background ai personaggi e renderli ancora più vivi e reali. Se in _BB_ assistevano alla folle corsa di un uomo verso **I**l potere e l'auto distruzione, qui la situazione cambia. Jimmy è un moderno _Amleto_ in continua lotta con se stesso; essere se stesso e fare del _male_ a chi lo circonda, oppure reprime la sua natura a favore del fratello e di Kim. Ed è per questo che la droga e la criminalità **N**on sono più centrali alla vicenda, ma restano sullo sfondo(riportati in primo piano di tanto in tanto dalla story line di Mike) lasciando emergere i rapporti famigliari/amorosi di Jimmy. Infatti personaggi secondari come Kim stanno assumendo sempre maggiore peso nell'economia del racconto, tant'è che in alcuni episodi si ha l'impressione che la vera protagonista della vicenda sia lei. L'avvocatessa tanto cara a Jimmy incarna alla perfezione la sua voglia di redenzione e indipendenza. La speranza in un futuro migliore, con un'identità propria e nuova, la ricerca di una carriera lontana dall'ombra del fratello e della HHM, un lavoro in cui si possa essere di aiuto a **G**li altri essendo finalmente liberi di essere se stessi.

Dall'altro lato abbiamo Chuck, personaggio duro, ma tutto sommato giusto. Nella prima stagione capiamo che il **S**uo comportamento severo nei confronti del fratello era necessario per far crescere Jimmy e allontanarlo dalla brutta strada che stava intraprendendo. Ora invece, sarà perché gli occhi dello spettatore tendono a coincidere sempre più con quelli di Jimmy o per alcuni flashback introduttivi che illustrano meglio il rapporto tra i due fratelli, fatto sta che in questa seconda stagione si ha la netta sensazione che la severità di Chuck non sia dettata dall'amore verso il fratello, ma da un vero e proprio odio. Un astio che si è trascinato nel tempo, forse motivato da un **B**riciolo d'invidia per la libertà di Jimmy, che sfocerà in una vera e propria faida famigliare.

A far i conti con tutti questi personaggi _forti_, c'è lui, il nostro Jimmy, non nobile, non ricco, coraggioso ancor meno, che si è dunque **A**ccorto, d'essere, in quella società, come un vaso di terra cotta, costretto a viaggiar in compagnia di molti vasi di ferro, una pecora circondata da lupi. Una pecora che è stata posta difronte ad una scelta. Essere o non essere, lupo o pecora, Saul o Jimmy. Ed è proprio qui che emerge l'unico grande difetto di Better **C**all Saul; lo spettatore conosce benissimo cosa sceglierà Jimmy.

**K**  

[1]: /serie-tv/better-call-saul/
