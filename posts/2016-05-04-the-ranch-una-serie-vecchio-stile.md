---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*A_3dUywY4psxJcGcJ28Tdg.jpeg
title:  "The Ranch, una serie vecchio stile"
date:   2016-05-04
author: Raffaele
categories:
    - serie-tv
    - netflix
permalink: /serie-tv/the-ranch-una-serie-vecchio-stile/
---

Quando ho aperto questo blog, mai avrei pensato di ritrovarmi qui a scrivere di una sitcom, genere sicuramente importantissimo e legato al passato telefilmico di tutti noi(penso alla storica _Friends_ o alla più moderna _How i Met Your Mother_), ma per l'appunto passato. Negli ultimi anni le serie TV sono cresciute sia in numero che in qualità ed hanno mutato profondamente la propria natura. Evoluzione che ha colpito in particolar modo il genere comico; basta pensare alla nuova generazione di comedy(_Girls, Silicon Valley, New Girl_ giusto per fare qualche nome) che sta spopolando sui nostri schermi, o alle ancora più innovative [**_sadcom_**][1] come [_Flaked][2], [Bojack Horseman][3] o You're The Worst_. Ma nonostante questa incessante trasformazione le sitcom sono ancora vive e vegete e **_The Ranch_** è la dimostrazione di come uno show rigorosamente vecchio stile possa intrattenere ancora oggi.

Sin dalle prime scene l'intento dei due autori ci appare chiaro; rispolverare gli elementi classici della TV anni '70 fondendoli con le più recenti tecniche narrative. Così l'impostazione verticale cede il posto ad una struttura unitaria, una narrazione orizzontale che collega tra di loro tutti gli episodi di questa prima stagione(anche se a Netflix piace chiamarla parte). Al centro della narrazione ci sono i Bennett, la classica famiglia disfunzionale a cui, dopo anni di serialità americana, siamo più che abituati, e i loro tentativi di ricostruire gli equilibri famigliari resi ancora più instabili dal ritorno del _figliol prodigo_ Colt. Caratteristica peculiare della serie è l'età(anagrafica) dei protagonisti, non più adolescenti, bensì adulti costretti a far i conti con il proprio _Peter Pan_ interiore. Quasi come se gli autori volessero proporre una versione agreste e più scanzonata di [_LOVE_][4]. Tutto questo permette alla serie di avere un tono e una comicità irriverente e senza peli sulla lingua. Per intenderci, i riferimenti alla sfera sessuale si sprecano, ma nel corso delle puntate lo spettatore incontrerà anche molte battute sulla politica americana e una forte critica alle nuove generazioni troppo legate alla tecnologia.

Se in linea di massima **_The Ranch_** riesce ad equilibrare nel giusto modo la tradizione con l'innovazione va anche detto che in alcuni casi gli autori restano troppo ancorati al passato. Le risate in sottofondo riescono ad infastidire come poche cose lo spettatore; non solo sono troppo _prepotenti_, ma in alcuni casi sono buttate lì giusto per. Anche la regia è troppo statica, si certo, ci troviamo difronte ad una sitcom a camera fissa, ma passare dai piani sequenza di [_Dareevil_][5] e _Better Call Saul_ alla totale staticità di _The Ranch_ non è proprio l'ideale. Fortunatamente dopo un paio di episodi ci si fa l'abitudine e quasi non ci si fa caso. Quello che infastidisce realmente non so questi piccoli difetti(più che difetti si tratta di un'avversione innata verso il genere da parte di chi scrive) bensì la pessima gestione della comicità nei primi episodi. Durante la visione delle prime due puntate sembra di assistere ad un remake di un cinepanettone in salsa Western. La cosa che mi fa più arrabbiare è che dopo la comicità migliora e anche tanto; si ha quasi l'impressione che le prime puntate siano state aggiunte solo per allungare un po'il brodo. Un vero peccato.

P.S.  
Nonostante gli ultimi annunci di Netflix e sui spot sulla prima _TV globale_ risulta piuttosto evidente la natura fortemente americana dello show. D'altronde, cosa c'è di più americano che un Cowboy?  

[1]: http://www.vulture.com/2015/09/rise-of-the-sadcom.html
[2]: /serie-tv/flaked/
[3]: /serie-tv/bojack-horseman-storia-un-rinunciatario/
[4]: /serie-tv/love/
[5]: https://www.youtube.com/watch?v=Q0CvkiPS5Ks
