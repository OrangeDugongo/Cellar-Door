---
title: Previsioni inesatte  -  Golden Globes
author: Raffaele
date: 2016-03-25
categories:
    - cinema
    - serie-tv
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*-nofcOaRqOscTqJu9tawBQ.jpeg
permalink: /cinema/previsioni-inesatte
---

#### Cinema

**Miglior film drammatico  
**Carol, regia di Todd Haynes  
Mad Max: Fury Road, regia di George Miller  
[Revenant — Redivivo][1] (The Revenant), regia di Alejandro González Iñárritu  
Room, regia di Lenny Abrahamson  
Il caso Spotlight (Spotlight), regia di Tom McCarthy

Non c'è un vero e proprio favorito quindi è difficile prevedere come andrà a finire; Iñárritu con Revenant potrebbe riuscire in quello che ha fallito dodici mesi fa, Carol sembra fatto apposta per raccogliere premi mentre Spotlight è da Venezia che fa parlare bene di se.  
**Chi vincerà: Iñárritu con Renevant.**

**Miglior film comedy o musical  
[**La grande scommessa][2] (The Big Short), regia di Adam McKay  
Sopravvissuto — The Martian (The Martian), regia di Ridley Scott  
Joy, regia di David O. Russell  
Spy, regia di Paul Feig  
Un disastro di ragazza (Trainwreck), regia di Judd Apatow

Qui la partita sembra abbastanza sbilanciata verso The Martian, ma anche La Grande Scommessa sembra avere buone possibilità. Qualche possibilità anche per Joy di O. Russell che ha già trionfato qualche anno fa in questa categoria.  
**Chi vincerà: Joy di David O. Russell**

**Miglior regista  
**Todd Haynes — Carol  
Tom McCarthy — Il caso Spotlight (Spotlight)  
George Miller — Mad Max: Fury Road  
Ridley Scott — Sopravvissuto — The Martian (The Martian)  
Alejandro González Iñárritu — Revenant — Redivivo (The Revenant)

Non ho visto ancora Revenant, ma da quello che si è visto nei trailer Iñárritu sembra avere la vittoria in tasca. Occhio però alle vecchie guardie, Miller su tutti.  
**Chi vincerà: Alejandro González Iñárritu.**

**Migliore attrice in un film drammatico  
**Cate Blanchett — Carol  
Brie Larson — Room  
Rooney Mara — Carol  
Saoirse Ronan — Brooklyn  
Alicia Vikander — The Danish Girl

In questa categoria sono abbastanza impreparato ma a naso punterei sulla vittoria della Blanchett.  
**Chi vincerà: Cate Blanchett.**

**Miglior attore in un film drammatico  
**Bryan Cranston — L'ultima parola — La vera storia di Dalton Trumbo (Trumbo)  
Leonardo DiCaprio — Revenant — Redivivo (The Revenant)  
Michael Fassbender — Steve Jobs  
Eddie Redmayne — The Danish Girl  
Will Smith — Zona d'ombra (Concussion)

Abbastanza favorito Leo, che con i globi non ha mai avuto particolari problemi. A dargli un po' di fastidio c'è Fassbender, che con il suo Jobs ha incantato molti. Attenzione anche a Cranston che ha già trionfato in passato ma nella sezione serie TV.  
**Chi vincerà: _Walter White_ permettendo la vittoria sarà di DiCaprio.**

**Migliore attrice in un film commedia o musicale  
**Jennifer Lawrence — Joy  
Amy Schumer — Un disastro di ragazza (Trainwreck)  
Melissa McCarthy — Spy  
Maggie Smith — The Lady in the Van  
Lily Tomlin — Grandma

Qui, per me, c'è solo Jen. Tutto il resto è noia.  
**Chi vincerà: Jennifer Lawrence.**

**Miglior attore in un film commedia o musicale  
**Christian Bale — La grande scommessa (The Big Short)  
Steve Carell — La grande scommessa (The Big Short)  
Matt Damon — Sopravvissuto — The Martian (The Martian)  
Al Pacino — La canzone della vita — Danny Collins (Danny Collins)  
Mark Ruffalo — Teneramente folle (Infinitely Polar Bear)

In vantaggio il marziano Matt Damon, seguono, staccati di poco, i due attori de La Grande Scommessa.  
**Chi vincerà: Christian Bale, attore di tutt'altro pianeta.**

**Miglior film d'animazione  
**Anomalisa, regia di Charlie Kaufman  
Il viaggio di Arlo (The Good Dinosaur), regia di Bob Peterson  
Inside Out, regia di Pete Docter  
Snoopy &amp; Friends — Il film dei Peanuts (The Peanuts Movie), regia di Steve Martino  
Shaun, vita da pecora — Il film (Shaun the Sheep Movie), regia di Mark Burton e Richard Starzak

Quando fa Pixar fa la Pixar non ce ne per nessuno. In qualsiasi altra annata Inside Out avrebbe vinto facile, ma quest'anno no, quest'anno c'è anche il film in stop motion di Kaufman.  
**Chi vincerà: nonostante tutto Inside Out.**

**Miglior film straniero  
**Dio esiste e vive a Bruxelles (Le tout nouveau testament), regia di Jaco Van Dormael (Belgio)  
El club, regia di Pablo Larraín (Cile)  
Miekkailija, regia di Klaus Härö (Finlandia)  
Mustang, regia di Deniz Gamze Ergüven (Francia)  
Il figlio di Saul (Salu fia), regia di László Nemes (Ungheria)

La vittoria dell'Ungheria sembra abbastanza scontata, peccato non ci siano italiani in gara, Non Essere Cattivo meritava e anche molto.  
**Chi vincerà: Il figlio di Saul.**

**Migliore attrice non protagonista  
**Jane Fonda — Youth — La giovinezza (Youth)  
Jennifer Jason Leigh — The Hateful Eight  
Alicia Vikander — Ex Machina  
Helen Mirren — L'ultima parola — La vera storia di Dalton Trumbo (Trumbo)  
Kate Winslet — Steve Jobs

Qui si tifa Italia, ma la vittoria sembra saldamente in mano alla Vikander.  
**Chi vincerà: Alicia Vikander.**

**Miglior attore non protagonista  
**Paul Dano — Love &amp; Mercy  
Idris Elba — Beasts of No Nation  
Mark Rylance — Il ponte delle spie (Bridge of Spies)  
Michael Shannon — 99 Homes  
Sylvester Stallone — Creed — Nato per combattere (Creed)

Probabilmente questo sarà l'anno di Stallone anche se la vittoria di Idris Elba sarebbe un ottima ricompensa per il coraggio dimostrato da Netflix.  
**Chi vincerà: Sylvester Stallone.**

**Migliore sceneggiatura  
**Emma Donaghue — Room  
Tom McCarthy, Josh Singer — Il caso Spotlight (Spotlight)  
Charles Randolph e Adam McKay — La grande scommessa (The Big Short)  
Aaron Sorkin — Steve Jobs  
Quentin Tarantino — The Hateful Eight

Il Caso Spotlicht sembra favorito, ma Quentin è sempre Quentin.  
**Chi vincerà: Quentin Tarantino.**

**Migliore colonna sonora originale  
**Carter Burwell — Carol  
Alexandre Desplat — The Danish Girl  
Ennio Morricone — The Hateful Eight  
Daniel Pemberton — Steve Jobs  
Ryūichi Sakamoto e Alva Noto — Revenant — Redivivo (The Revenant)

Qui tutti bravi, ma non dovrebbe esserci partita comunque, il migliore è il nostro Morricone.  
**Chi vincerà: Ennio Morricone.**

**Migliore canzone originale  
**Love Me Like You Do (Max Martin, Savan Kotecha, Ali Payami, Tove Nilsson, Ilya Salmanzadeh) — Cinquanta sfumature di grigio (Fifty Shades of Grey)  
One Kind of Love (Brian Wilson, Scott Bennett) — Love &amp; Mercy  
See You Again (Justin Franks, Andrew Cedar, Charlie Puth, Cameron Thomaz) — Fast and Furious 7 (Furious 7)  
Simple Song#3 (David Lang) — Youth — La giovinezza (Youth)  
Writing's on the Wall (Sam Smith, Jimmy Napes) — Spectre

Certo si tifa Italia, ma bisogna anche essere realisti. Fast and Furious è un film che fa acqua da tutte le parti, non ha una storia da raccontare, non ha niente, ma trattenere le lacrime negli ultimi cinque minuti è quasi impossibile.  
**Chi vincerà: See You Again.**

#### Serie TV

**Miglior serie drammatica**
Empire
Il Trono di Spade (Game of Thrones)
[Mr. Robot][3]
[Narcos][4]
Outlander

Il vero [pezzo grosso][5] di quest’annata manca e questo mi fa arrabbiare non poco. Tra i candidati Game of Thrones parte con un leggere vantaggio, ma la serie Netflix e Mr. Robot sono subito dietro pronte ad approfittarne.
**Chi vincerà: Narcos.**

**Migliore attrice in una serie drammatica**
Caitriona Balfe — Outlander
Viola Davis — Le regole del delitto perfetto (How to Get Away With Murder)
Eva Green — Penny Dreadful
Taraji P. Henson — Empire
Robin Wright — House of Cards — Gli intrighi del potere (House of Cards)

Quasi tutte le nominate recitano in serie che non seguo quindi boh.
**Chi vincerà: non ne ho la più pallida idea.**

**Miglior attore in una serie drammatica**
Jon Hamm — Mad Men
Rami Malek — Mr. Robot
Wagner Moura — Narcos
Bob Odenkirk — [Better Call Saul][6]
Liev Schreiber — Ray Donovan

Probabilmente questa è la categoria più combattuta. Tutti meriterebbero la vittoria ed è difficile scegliere, ma prevedo una lotta a due Rami Malek contro Wagner Moura.
**Chi vincerà: Rami Malek.**

**Miglior mini-serie o film per la televisione**
American Crime
American Horror Story: Hotel
[Fargo][7]
Flesh and Bone
Wolf Hall

Qui non dovrebbe esserci partita. Fargo è quanto di meglio la serialità oggi possa offrire. La sua vittoria non sarebbe solo giusta, ma anche doverosa.
**Chi vincerà: Fargo.**

**Migliore attrice in una mini-serie o film per la televisione**
Lady Gaga — American Horror Story: Hotel
Sarah Hay — Flesh and Bone
Felicity Huffman — American Crime
Queen Latifah — Bessie
Kirsten Dunst — Fargo

Va bene chiunque, ma non Lady Gaga.
**Chi vincerà: Kirsten Dunst.**

**Miglior attore in una mini-serie o film per la televisione**
Idris Elba — Luther
Oscar Isaac — Show Me a Hero
David Oyelowo — Nightingale
Mark Rylance — Wolf Hall
Patrick Wilson — Fargo

Ripeto Fargo è di un altra categoria.
**Chi vincerà: Patrick Wilson.**

Probabilmente vi sarete già accorti la totale assenza delle comedy, il motivo è semplice, sono un ignorante in materia e preferisco non esprimermi. Naturalmente faccio il tifo per i nerd di Silicon Valley ma niente di più.

**Buoni Golden Globes a tutti.**

[1]: /cinema/revenant/
[2]: /cinema/la-grande-scommessa/
[3]: /serie-tv/mr-robot-breaking-bad-parliamone/
[4]: /serie-tv/xmas-binge-watching/
[5]: /serie-tv/the-leftovers/
[6]: /serie-tv/better-call-saul/
[7]: /serie-tv/twin-peaks-fargo-andata-ritorno/
