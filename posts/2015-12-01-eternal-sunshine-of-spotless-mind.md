---
title: Eternal Sunshine of the Spotless Mind
author: Raffaele
date: 2015-12-01
categories:
    - cinema
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*20RkGXfUEFugyySC1n6_xg.jpeg
permalink: /cinema/eternal-sunshine-of-the-spotless-mind/
---

# Eternal Sunshine of the Spotless Mind

Raccontare è sottovalutato, ogni giorno raccontiamo qualcosa alle persone che ci circondano, lo facciamo così spesso che ci dimentichiamo di quanto sia difficile. Ti sarà sicuramente capitato di raccontare una barzelletta e di dimenticare un dettaglio, un dettaglio che lì per lì sembrava superfluo, ma capire solo alla fine quanto fosse importante. Per colpa di quel dettaglio mancato il tuo pubblico non ha riso. Altre volte ti sei soffermato così tanto su quel dettaglio che la battuta è uscita allo scoperto prima del previsto. Gli sceneggiatori lo chiamano _set-up_, disseminare indizi nell'arco del film, posizionarli in modo che lo spettatore possa vederli ma non notarli, perché gli indizi devono restare nascosti fino al grande colpo di scena, il_play-off_. Gli sceneggiatori capaci riescono a mettere su un buon _set-up_ senza insospettire lo spettatore che realizzerà l'importanza di quei dettagli solo quando il colpo di scena è già servito. Poi ci sono i geni, i maestri della sceneggiatura, riescono ad integrare così bene il _set-up_ nella storia che lo stesso dettaglio assume significati completamente diversi prima e dopo aver visto il finale. E tu non puoi far altro che restare con quell'espressione da abete e pensare: "_Quindi è così… e io che pensavo… Mi sento così stupido, pensavo di sapere tutto e invece non ci avevo capito niente_".

Charlie Kaufman è così, ti illude, ti fa credere che la storia sta prendendo una piega, invece va a finire in tutt'altra direzione. I film che ha sceneggiato mi piacciono tutti ma ce ne uno a cui sono particolarmente legato, _Eternal Sunshine of the Spotless Mind_. Non l'hai mai sentito nominare? Non mi sorprende, è un film abbastanza di nicchia che in Italia per qualche lampo di genio dei traduttori è stato intitolato _Se mi Lasci ti Cancello_, si lo so sembra un titolo di un cinepanettone, ma il film merita, merita veramente.

E_ternal Sunshine of the Spotless Mind_ è onirico, nel corso del film la linea di demarcazione tra sogno e realtà diventa sempre più sottile, quasi impercettibile. Ma non ci sono solo sogni e ricordi, c'è anche l'amore. _Eternal Sunshine of the Spotless Mind_ è romantico, ma non romantico e basta, è romantico come solo i film francesi sanno essere. Non è il solito: _"Ma quanto stiamo bene insieme, sei la mia anima gemella ti amo e ti amerò per sempre"_no, questo è un amore autentico, non l'amore stereotipato dei biglietti di San Valentino. I protagonisti si muovono in una concezione del tempo quasi Nietzschiana, dove sono destinati a commettere gli stessi sbagli e gli stessi errori in eterno. Il _vissero tutti felici e contenti_ viene sostituito dal _prima o poi finisce_.

_Meno male che era un film romantico._

Silo so, può sembrare quasi in invito a non innamorarsi, il film sembra dirci: _"è da stupidi recarsi tanto dolore consapevolmente"_, ma andando oltre l'apparenza ci si accorge che è un si alla vita, un si all'amore. La consapevolezza di questa fine non è un intralcio all'amore ma un incentivo a vivere fino in fondo ogni momento e ogni piccola emozione irripetibile che solo una relazione ci sa regalare. Tralasciare quello che sarà e concentrassi sul qui e ora, perché l'amore è come un viaggio dove non conta arrivare, ma solo viaggiare.  
