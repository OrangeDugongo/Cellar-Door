---
title: Batman v Superman
author: Raffaele
date: 2016-03-24
categories:
- cinema
- hero
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*Lu-8U4eamm0njzVcnqMuQg.jpeg
permalink: /cinema/batman-v-superman/
---

Fin da piccolo i supereroi non mi sono mai piaciuti tantissimo, erano tropo lontani. L'unico per cui riuscivo a provare un briciolo d'affetto era Batman. Crescendo ho scoperto il_ lato umano_ di questi eroi e da _buon nerd_mi sono interessato sempre di più alle loro vicende, guardando sempre con un briciolo di attenzione in più il caro vecchio uomo pipistrello. Poi la Disney ha acquistato la _Marvel_ e insieme hanno reinventato il concetto stesso di cinecomics. Eroi colorati e pupazzosi che di adulto avevano ben poco. L'unica ancora di salvezza per vedere qualcosa di più adulto al cinema la riponevo in _man of steel_, che si è rivelato un vero e proprio buco nell'acqua. Ed è proprio per questo che non riponevo nessuna speranza nel suo seguito, che nonostante mi abbia deluso in senso stretto mi lascia bene sperare per il futuro.

Ilfilm si gioca le sue carte migliori in apertura con dei fantastici titoli di testa che mostrano allo spettatore la morte dei coniugi _Wayne_. Snyder, rifacendosi al suo _Watchmen_ ci regala una carrellata di slow motion molto fedele alla story-board del fumetto; un ottimo stratagemma per accattivarsi i fan più legati alle vicende cartacee del personaggio. Purtroppo il film non continua come aveva iniziato e si perde in una serie di scene, tutte scollegate tra di loro(ci sono scene puramente casuali) saltando qua e là in giro per il mondo. Lo spettatore ha la sensazione di non star capendo bene ciò che succede sullo schermo, ma continua la visione fiducioso in una spiegazione futura che si verificherà solo parzialmente. Tutto ciò evidenzia l'incapacità degli autori nel riuscir a far coesistere sullo schermo i due eroi. Situazione per niente incoraggiante se si guarda alla futura **_Justice league_**, quando Snyder dovrà gestire sei personaggi contemporaneamente.  
Tra i due eroi a catturare maggiormente l'attenzione è decisamente il nuovo Batman di Ben Afflek. L'attore dando uno schiaffo morale a tutti i fan che tanto l'avevano criticato durante il casting ci regala una nuova incarnazione dell'uomo pipistrello che certo non sfigura di fianco a Bale e Keaton. Ci troviamo difronte ad un nuovo Batman legato con un doppio filo a quello di Nolan, ma più anziano e logorato, non tanto nel corpo quanto nello spirito, da tanti anni di battaglie. Una versione ancora più cupa e violenta del cavaliere oscuro di Miller, tant'è che Batman non si farà problemi ad usare armi da fuco, marchiare e qualche volta anche uccidere i propri nemici. Ed è proprio questa crescente brutalità ad innescare lo scontro tra i due eroi.  
Ad uscirne invece con le ossa rotte è Superman, che nonostante il tentativo di renderlo più umano sviluppando il suo rapporto con Lois e la madre Marta, risulta ancora troppo distante e lontano dallo spettatore, ma in generale dalla condizione umana, tanto da non riuscire a comprendere fino in fondo le conseguenze delle sue azioni.

<blockquote class="twitter-tweet" data-lang="it"><p lang="it" dir="ltr">Perché a un certo punto Superman traina una barca? <a href="https://twitter.com/hashtag/BatmanvSuperman?src=hash">#BatmanvSuperman</a></p>&mdash; Raffaele Mignone (@Orange_dugongo) <a href="https://twitter.com/Orange_dugongo/status/712779859630682112">23 marzo 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Nella seconda parte il film recupera in consistenza, ma le fondamenta sono troppo deboli per riuscire a sostenere le aspettative dell spettatore che, complice anche i trailer spoilerosi, uscirà dalla sala decisamente deluso. Gli autori non hanno fatto niente per rendere migliore un film nato male. Dopo l'insuccesso di **_man of steel_** la DC ha deciso di accantonare un secondo capitolo dedicato all'uomo d'acciaio per puntare con decisione su **_Batman v Superman_** con la speranza di (ri)lanciare un universo condiviso. Una decisione affrettata e sbagliata dettata dalla necessità d'inseguire la Marvel(e quindi il mercato miliardario che ha creato) anche a costo di mettere in secondo piano la qualità. In questo film Snyder avrebbe dovuto continuare la story line di Clark, introdurre Batman e farlo scontrare con il protagonista; portare sullo schermo i due villan, Lex Luthor e Doomsday(e naturalmente farli scontrare con i buoni), far entrare in scena **Wonder Woman**, gettare le basi per la _Justice league_ e quindi presentare allo spettatore **Aquaman**, **Flash** e **Cyborg** e infine aprire nuovi percorsi narrativi per il futuro. Un sacco di obbiettivi eterogenei tra di loro, che nonostante tutto sono centrati dal regista anche se in modo molto discutibile.

Giudicare ora negativamente **_Batman v Superman_** non sarebbe giusto. Oggettivamente è un film con dei grossi limiti, ma paga, suo malgrado, per la natura transitoria, costretto a fare il lavoro sporco, traghettare l'universo DC nato(e morto) con _man of steel_ verso un nuovo universo più compatto e popoloso pronto a battagliare con quello Marvel, universo che intravederemo solo questa estate con **_Suicide Squad_**. Ora come ora andrebbe, più che altro, giudicato l'operato di Snyder, Goyer e degli altri autori che sembrano non ancora pronti(leggi incapaci) di operare con una vasta mole di personaggi.

## Spoiler

**Da qui in poi ci saranno degli spoiler, anche se c’è veramente poco da spoilerare. Se non hai ancora visto il film ti consiglio di non continuare la lettura.**

Per quanto non abbia apprezzato molto il finale alla [Inception][1], l’ho trovato molto interessante perché apre le porte a molte possibili evoluzioni. Dando per scontato che la morte di Superman non sia definitiva e che il personaggio ritornerà, sono abbastanza convinto che ciò non accadrà in tempi brevi, anzi sono quasi certo che la DC svilupperà il suo universo condiviso rinunciando al suo personaggio più forte, ma anche più difficile da gestire.

[1]: /cinema/apologia-christopher-nolan/
