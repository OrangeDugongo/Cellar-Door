---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*k8sTfWTu0Y8WpaotaEvi-A.jpeg
title: "The Truman Show: il palcoscenico della vita"
date:   2015-12-18
author: Raffaele
categories:
    - cinema
permalink: /cinema/the-truman-show/
---

Il più grande reality show di tutti i tempi, un solo concorrente, nessuna eliminazione, nessun premio finale, solo ventiquattro ore di diretta al giorno per più di trent'anni. Ecco cos'è _The Truman Show._

Truman è un trentenne con una vita normale; sposato con una dolce infermiera bionda, lavora in una compagnia d'assicurazioni e ogni mattina esce di casa con il sorriso sulle labbra. Quella che a primo acchito potrebbe sembrare una vita come tante in realtà è un'immensa mesa in scena. Truman, abbandonato dai genitori appena nato, è stato adottato da Christof e dal suo studio televisivo che ha costruito intorno alla vita del piccolo Truman il più grande reality di tutti i tempi. La città in cui vive è un grande set televisivo, il cielo è finto, il sole è solo un grande faro, tutto in questo _universo parallelo_ è controllato da Christof(nome non casuale), ma soprattutto è finto.

_Quindi&nbsp;è&nbsp;tutto&nbsp;finto?_

No, non tutto. Truman è vero, ed è proprio per questo che gli spettatori di tutto il mondo amano il Truman Show. Truman se pur frutto di una folle menzogna è più vero di chiunque altro. Lui non indossa maschere pirandelliane, non finge, non ha bisogno di parlare alle spalle, lui è autentico. Uno straordinario Jim Carrey, con la sua faccia da bambino, sarà costretto a lottare contro tutti e tutto per uscire dalla [caverna][1] e vivere finalmente la sua vita vera.

Peter Weir narrandoci le vicende di Truman tratta numerose tematiche; critica, non solo al mondo della televisione, schiavo degli sponsor e della pubblicità, ma anche i telespettatori che per anni seguono le vicende del protagonista senza mai chiedersi se tutto ciò sia giusto. Inoltre la pellicola mette in scena un percorso di crescita e conoscenza di se stessi che lo spettatore percorre insieme al protagonista, e lo porta ad interrogarsi sul vero significato di libertà e verità.

[1]: https://it.wikipedia.org/wiki/Mito_della_caverna
