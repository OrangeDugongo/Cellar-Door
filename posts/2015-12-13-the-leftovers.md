---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*oMwTHdyG0ijPC13RZqWniQ.jpeg
title:  The Leftovers
date:   2015-12-13
author: Raffaele
categories:
    - serie-tv
permalink: /serie-tv/the-leftovers/
---

Dare una definizione universale di bellezza è impossibile. Quello che piace a me non è detto che debba piaccia anche a te mio lettore. Non c'è niente di male, siamo tutti diversi, ognuno con i propri interessi, con le proprie inclinazioni e i propri gusti; e va bene così, il bello è proprio questo. A volte purtroppo non giudichiamo con la nostra testa e ci facciamo influenzare dalla massa. La serie di cui ti parlerò oggi non è tra le più famose, gli ascolti sono abbastanza bassi e ogni anno il rinnovo è incerto; eppure quei pochi fan sono pronti a manifestare sotto la sede HBO, evidentemente un motivo ci sarà.

Il14 ottobre il 2% della popolazione mondiale è scomparsa nel nulla, svanita, andata per sempre. Che fine hanno fatto? Torneranno? Perché sono scomparsi? Tutte domande lecite che non avranno mai risposte. La serie, diversamente dalle aspettative dei più, non indaga le cause di questo evento, ma predilige le reazioni di chi l'ha scampata. La prima stagione è basata sull'omonimo romanzo di Tom Perrotta, produttore della serie, ci mostra la nuova vita dei leftovers, madri che hanno perso i figli, figli che hanno perso i genitori, religioni e pseudo religioni disposte a tutto pur di aiutare i fedeli a dimenticare e superare l'evento. A rendere la situazione ancora più instabile ci pensano i Guilty Remnant, una setta di uomini in bianco che hanno fatto voto di silenzio e passano le giornate a fumare, bada bene, non per piacere ma perché è la loro fede. La setta è la testimonianza vivente del 14 ottobre ed è loro compito impedire che quella data venga dimenticata. Inutile dire che tutto ciò porterà allo scontro tra chi vuole andare avanti e i GR. A fare da cuscinetto tra le due fazioni c'è Kevin Garvey, un poliziotto di Mapleton, la cui situazione famigliare è molto frammentata.

Inquesto primo blocco di episodi i personaggi sono solo tratteggiati, degli archetipi che potrebbero rappresentare chiunque. Infatti è molto facile per lo spettatore rispecchiarsi in una di queste categorie. Poi arriva la seconda stagione e cambia tutto, un reboot creativo. Nuova città, Jarden, l'unico luogo al mondo in cui non ci sono state dipartite; nuovi personaggi, tra cui spicca soprattutto John e la sua famiglia, ma il cambiamento più grande è nello stile. I personaggi che prima erano solo abbozzati ora sono vivi, ognuno con la sua storia e ognuno con i suoi demoni. In questa seconda stagione Damon Lindeloft(si lo stesso di Lost)cambia tutto per non cambiare niente, perché The Leftovers è sempre lo stesso, fugace, indefinito, pieno di dubbi e di interrogativi a cui seguono sollo delle ipotesi che primo o poi verranno confutate.

Durante questo rinascimento televisivo che stiamo vivendo le serie TV non stanno crescendo solo in numero, ma anche di qualità, ma nonostante questo The Leftovers resta unica e inimitabile. A questo punto mio lettore ti aspetteresti delle motivazioni a sostegno di questa mia affermazione, ma non ne troverai.

_Perché?_

Semplice, perché non le conosco neanche io. Ho visto due stagioni, ma alla domanda: di cosa parla The Leftovers? Non posso far altro che sorridere come un ebete e fare spallucce. Puoi passare l'intera vita a spiegare ad un ateo perché è così importante Dio, ma se lui non ha fede non capirà mai. Con _The Leftovers_ è lo stesso, solo se hai fede e credi nella serie puoi godertela e forse capirci qualcosa.  
