---
title: Una bellissima scatola vuota
author: Raffaele
date: 2016-01-23
categories:
    - cinema
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*K2k6LekqJZkmjeIoMTgI7A.jpeg
permalink: /cinema/revenant/
---

Dire che attendevo con ansia questo film sarebbe alquanto riduttivo. Il cinema di Alejandro González Iñárritu mi ha sempre interessato e colpito, sin dall'esordio con _Amores Perros_ ho sempre trovato i suoi film innovativi e mai banali, ma l'anno scorso con [_Birdman_][1] qualcosa è cambiato. _Birdman_, per me, non è stato solo un film, ma una vera e propria esperienza di vita che mi ha trasformato e cambiato. Proprio per questo quando iniziarono a circolare le prime indiscrezioni sul nuovo lavoro del regista messicano il mio interesse crebbe a dismisura. Si andava prospettando un viaggio spirituale alla ricerca di se stessi, certo non novità assoluta nel panorama cinematografico, ma comunque un argomento interessante, aggiungici la presenza di DiCaprio, non il mio attore preferito, ma capace di regalare personaggi indimenticabili ed ecco che l'hype è servito.

Si spengono le luci, cala il silenzio in sala e parte la proiezione. Bastano pochi minuti per rendersi conto che il vero protagonista della vicenda non è DiCaprio(e neanche [l'orso][2]), ma è la natura a dominare incontrastata la scena. Niente green screen, niente luci artificiali, solo la natura messa a nudo nella sua semplicità e nella sua crudeltà. Ogni locations è unica e immediatamente riconoscibile, si stampa nella mente dello spettatore e lo incanta. Questi luoghi inesplorati sono resi ancora più affascinati dai giochi di luce creati da _Chivo. _il lavoro Emmanuel Lubezki è sensazionale e non mi sorprenderebbe se gli valesse il terzo oscar consecutivo. Ma ancora una volta a sorprendere di più è la regia di Iñárritu che abbandona l'unico, finto, piano sequenza, ma continua a riprendere a distanza ravvicinata. l'obiettivo si appanna col respiro dei protagonisti, si sporca di sangue e neve, non teme di seguire l'azione anche sott'acqua. L'inquadratura segue l'occhio curioso del regista e si fa personaggio aggiunto alla scena.  
L'interpretazione di DiCaprio è straordinaria, annienta completamente la sua personalità per entrare nei panni di Hugh Glass. Okay non ha molte battute, ma non ne ha bisogno, riesce a esprimere tutte le sue emozioni con la comunicazione non verbale. Tom Hardy non è da meno e restituisce la migliore interpretazione della sua carriera. I suoni e la colonna sonora che si fonde con la natura lascia senza parole, il montaggio, i costumi, il trucco, tutti gli ingranaggi del film, presi singolarmente, funzionano alla perfezione eppure, paradossalmente quando diventano tutt'uno collassano su se stessi. La storia sa di già visto e non aggiunge niente di nuovo al genere, uscendo dal cinema avevo l'impressione che l'unica ragione d'essere di questo film è l'ego. Iñárritu e la sua crew sembrano gridare al mondo: "Ammirateci! Guardate quanto siamo bravi." ma era necessario realizzare due ore e mezzo di film spendendo 135 milioni di dollari per dire qualcosa che già sapevamo?

Revenant è un bellissimo contenitore, decorato in ogni piccolo dettaglio, fatto con materiali preziosi, bellissimo da vedere, in una sola parola perfetto. Poi quando lo vai ad aprire ecco che arriva la delusione, è vuoto.

[1]: /cinema/birdman/
[2]: http://www.theguardian.com/film/2016/jan/19/leonardo-dicaprio-revenant-bear-attack-stuntman-blue-suit
