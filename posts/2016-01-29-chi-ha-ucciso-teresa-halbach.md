---
title: Chi ha ucciso Teresa Halbach?
author: Raffaele
date: 2016-01-29
categories:
    - serie-tv
    - netflix
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*C_ZQRlIkYaxLflRXd2JnYg.png
permalink: /serie-tv/making-a-murderer-ucciso-teresa-halbach/
---

All'inizio ero un po' riluttante all'idea, iniziare una nuova (docu) serie TV dedicata ad una straziante vicenda giudiziaria non mi sembrava la mossa giusta da fare. Sostanzialmente ero vittima di un pregiudizio. L'incipit di _Making a Murderer_ mi ricordava tanto le infinite trasmissioni di cronaca nera che "allietano" i pomeriggi di tanti Italiani; ma ne parlavano tutti così bene, non poteva trattarsi di un delirio di massa, così alla fine mi sono convinto. Mai fatta scelta migliore.  
La serie ripercorre le paradossali vicende giudiziarie di Steven Avery, incarnazione perfetta della classe medio/bassa, condannato a 36 anni di prigione per uno stupro. Lui si dichiarò fin da subito innocente, ma per polizia, giudice e giurati il verdetto era uno; colpevole! Salvo poi scoprire, grazie al test del DNA, la sua innocenza. Steven fu rilasciato dopo 18 anni di ingiusta detenzione, era pronto a rifarsi una vita, pronto a incassare il suo risarcimento e lasciarsi tutto alle spalle, ma è proprio in questo momento che la realtà supera la fantasia. Dopo un anno di libertà Steven Avery viene accusato di nuovo, questa volta però non si tratta di stupro ma di omicidio.

Il&nbsp;lavoro svolto delle due autrici è stato lungo(10 anni di produzione) e faticoso, ma alla fine ha dato i risultati sperati. La serie svolge uno straordinario lavoro documentaristico sul sistema giuridico americano, fine sicuramente importante e nobile, ma che perde potenza man man che ci si allontana dai confini statunitensi. Per questo il grande merito dell'autrici sta nel riuscir a tenere sempre alta l'attenzione dello spettatore grazie ad un complesso sistema narrativo che va oltre il semplice documentario. Durante molte puntate si ha quasi la sensazione di guardare un crime drama, in altri momenti la serie sembra quasi diventare un legal e altre volte ancora un prision drama. La serie riprende tutte le strutture e le situazioni già note al pubblico seriale(l'abitazione degli Avery e le campagne del Wisconsin richiamano alla memoria la desolazione della Louisiana di _True Detective_) ma riesce ad aggiungervi ancora più drammaticità grazie ad una storia realmente accaduta. Forse l'unico vero difetto di _Making a Murderer_ è che a volte si allontana troppo dal documentario, non riscendo ad essere sempre oggettivo e presentando la vicenda da un determinato punto di vista.  
Ma tutto ciò resta solo sullo sfondo, perché a rendere veramente unica questa serie è l'impattato emotivo che riesce ad esercitare sui telespettatori. Rabbia per un sistema corrotto e poco vicino alle esigenze dei più poveri, dolore per un uomo che ha passato metà della sua vita dietro a delle sbarre. Ma anche disprezzo per i media e i giornalisti sempre pronti ad emettere condanne sommarie, compassione per le famiglie delle vittime e dei carnefici distrutte da questi terribili eventi, ma anche un po' di speranza in un futuro migliore. Una tempesta di sentimenti ed emozioni che alla fine passano in secondo piano, perché quello che resta nella testa è una fastidiosa domanda: **Se ci fossi stato io al suo posto?**  
