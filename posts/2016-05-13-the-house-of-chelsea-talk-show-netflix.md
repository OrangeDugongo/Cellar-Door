---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*wFGbVneAv0PDRY1Iyl8lCQ.jpeg
title:  "The House of Chelsea"
# subtitle: "What you can do with it!"
date:   2016-05-13
author: Raffaele
categories:
    - netflix
permalink: /netflix/the-house-of-chelsea-talk-show-netflix/
---

Dal lontano 1997 ad oggi Netflix ne ha fatta di strada. Partito come servizio di noleggio di DVD e videogiochi tramite internet, col tempo si è trasformato in un immenso catalogo di contenuti video a disposizione degli utenti dove e quando si vuole. A fare la fortuna dell'azienda è stata indubbiamente la sua prontezza a rispondere alle esigenze del mercato. E fu proprio per questo che nel 2013 i vertici dell'azienda decisero di scommettere su _House of Cards_, una serie TV originale, la _prima_ serie TV originale. E come spesso accade nella vita, la fortuna aiuta gli audaci; un successo straordinario di critica e pubblico, una delle migliori produzioni degli ultimi anni, a cui ne seguirono altre, tutte(o quasi), destinate ad entrare nell'olimpo delle serie TV. L'intuizione e il genio di Reed Hastings capirono che per portare Netflix al successo la piattaforma doveva evolvere, non poteva essere un catalogo da affiancare alla TV tradizionale, doveva diventare la nuova TV tradizionale. Ciò che a Netflix ancora mancava per competere con la televisioni classiche era un Talk Show. Ogni emittente televisiva americana ha il suo Talk Show, alcuni sono così famosi e radicati nella cultura contemporanea che sono&nbsp;trasmessi anche al di fuori dei confini americani. Il caso più eclatante è sicuramente quello di [_David Letterman_][1] e il suo _Late Show_ trasmesso in più di cinquanta paesi diversi. Record assoluto per un prodotto televisivo, o almeno lo era fino ad due giorni fa, perché ieri Netflix ha rilasciato contemporaneamente in 190(praticamente tutto il mondo cina esclusa), il suo nuovo Talk Show, **Chelsea**.

Lo show in buona parte rispetta gli standard del genere, con l'unica differenza che **Chelsea** non ha ne peli sulla lingua ne freni inibitori. Quindi la conduttrice non si farà problemi a parlare di argomenti solitamente tabù, insultare Trump e non ci penserà due volte prima di bere vino durante la trasmissione o salire sul tavolo, per lei queste sono tutte cose normali. Se da un lato tanta esuberanza può aiutare ad intrattenere lo spettatore, è anche vero che sul lungo periodo potrebbe annoiare. Soprattutto se si considera la tendenza di&nbsp;Chelsea&nbsp;a convogliare su se stessa tutta l'attenzione mettendo in secondo piano gli ospiti. Diciamo che in un talk non è proprio l'ideale. Ciò che rende veramente _speciale_ questo show, non è tanto il contenuto quanto la messa in onda. Per la prima volte Netflix rinuncia al _binge watching_ tanto caro agli utenti lasciando il posto ad una trasmissione giornaliera(mercoledì, giovedì e venerdì), ma d'altronde non potrebbe essere altrimenti. Lo spettacolo, per forza di cose deve parlare di argomenti recenti e quindi l'intervallo temporale tra la ripresa e la effettiva messa in onda deve essere ridotto al minimo. Caro lettore, capirai da te che tutto ciò non solo rappresenta una svolta pazzesca per le strategie di Netflix, ma segna un ulteriore avvicinamento verso spettacoli live. Purtroppo non è oro tutto ciò che luccica. Innanzitutto lo show è pensato quasi esclusivamente per il pubblico americano e inoltre non porta quella ventata di novità che ci si aspettava, anzi si trascina dietro problemi tipici della TV. Per esempio, tra un'ospitata e l'altra ci sono dei brevi sketch. Si certo, ruotano intorno al mondo di Netflix e diciamo che, almeno nelle intenzioni degli autori, dovrebbero far ridere, ma la verità è che ricordano tanto la pubblicità di cui pensavamo di eserci finalmente liberati. Spero sia solo una mia, sbagliata, impressione.

P.S.

Ho apprezzato molto il design dello studio e la presenza del cane, trasmettono l'idea di casa.  

[1]: https://www.youtube.com/watch?v=vUKOtbVnV4M
