---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*2omvzOFmOcYrCS-k-zNbGA.jpeg
title: Perché Cellar Door?
date:   2015-11-25
author: Raffaele
permalink: /cinema/perche-cellar-door/
---

Mio caro lettore forse ti chiederai perché _Cellar Door_ o almeno io al tuo posto lo farei, beh un famoso linguista ha definito _"Cellar Door"_ la più bella combinazione di parole della lingua inglese. Una bellezza intrinseca che supera di gran lunga parole come _say_, _sky_ e addirittura più bella di _beautiful_. Bellezza che, secondo il linguista, accresce se la si considera slegata dalla propria grafia e dal significato, una bellezza irresistibile se ci si limita al suo suono.

>Most English-speaking people… will admit that cellar door is "beautiful", especially if dissociated from its sense (and from its spelling). More beautiful than, say, sky, and far more beautiful than beautiful.

Per me invece la vera bellezza di *Cellar Door* risiede proprio nel suo significato.

_Si, va bene tutto quello che vuoi, ma che c'è di bello nella porta della cantina?_

Ed è che ti sbagli mio lettore, le cantine saranno anche tetre e piene di ragnatele(e di ragni), ma non puoi negare che sono anche piene di fascino e mistero. La cantina è il luogo del silenzio, dei bauli pieni di ricordi, delle cianfrusaglie che credevamo di aver perso, dei cimeli di famiglia. Tutte queste cose so lì, impolverate, in attesa che qualcuno le riprenda, mentre tu, mio lettore, sei dall'altro lato a fissare quella porta, perplesso e dubbioso, aprire o non aprire, con la consapevolezza che una volta aperta la _Cellar Door_ non ci troverai niente di buono, ma non lo saprai mai con certezza finché non ti farai coraggio ad aprire quella porta, poi chi sa, potresti trovarci qualche bella storia. A te la scelta.  
