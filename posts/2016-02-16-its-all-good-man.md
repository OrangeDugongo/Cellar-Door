---
title: It’s All Good Man
author:
 fullname: Raffaele
date: 2016-02-16
categories:
    - serie-tv
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*qd-e56L8jlksfMv8DpWZvw.jpeg
permalink: /serie-tv/better-call-saul/
---

La&nbsp;fine di una serie TV non è mai un bel momento, si può quasi dire che una parte di noi finisce con essa. Lo so, potrebbe sembrare un delirio, ma è così, alcune serie ti lasciano un vuoto difficile da colmare; Breaking Bad per me(e credo anche per molti altri) è stato proprio questo. Inutile negarlo, quando è stato annunciato lo spin-off la gioia è cresciuta a dismisura, ma insieme alla gioia è arrivato anche il terrore. Fin da quando ero piccino i miei genitori mi ripetevano che ci sono due modi di fare le cose; farle nel modo giusto o farle male, e poi aggiungevano: «E se le devi fare male è meglio che non le fai proprio». Ecco _Better Call Saul_ mi ha riportato alla memoria proprio questi momenti. Gilligan e soci potevano fare le cose per bene e sfornare un'altra pietra miliare della TV o farle male e infangare buon nome di _Walter White_. Dopo il doppio episodio pilota mi sembrava evidente che gli autori evasero scelto la seconda strada.  
Le inquadrature, l'atmosfera, le musiche, la fotografia, tutto riportava alla mente _Breaking Bad_. Avevo l'impressione di essere tornato indietro nel tempo, nella stessa Albuquerque dove avevamo lasciato _Walter_, ma prima del cancro e di tutto il resto; tanta, tanta malinconia, ma anche un senso d'incompiutezza. _Better Call Saul_ non aveva un anima propria, era una spin-off come tanti, non aveva niente da dire era un semplice escamotage per accontentare i fan e continuare a fare soldi. Ma questa è solo una bugia. La verità è che io, un po' come tutti noi, sono sempre pronto a giudicare prima del tempo e amo dire "Te l'avevo detto". _Better Call Saul_ in realtà è una grande serie TV, i primi episodi sono stati solo una breve parentesi, un ultimo saluto, l'addio di Gilligan alla sua creatura.

&gt; Skyler tu devi capire che ho fatto tutto questo per me. Mi piaceva farlo ed ero molto bravo.

**Qualche leggero spoiler sulla prima stagione di Better Call Saul e anche su Breaking Bad, anche se quest'ultimi sono ormai prescritti.**

Temporalmente le vicende di Jimmy avvengono prima dei fatti della serie madre, ma idealmente ne è il sequel. Dopo essersi visto chiudere in faccia tutte le porte della vita, _Walt_ vede in questa sua nuova _professione_ una via di sbocco. Per tutte le cinque stagioni il suo cambiamento ci viene mostrato come una scelta necessaria, come l'unica via percorribile, ma con l'ultimo episodio, [Fe][1][Li][2][Na][3], qualcosa cambia. Walt confessa alla moglie di aver messo su un impero della droga solo per proprio piacere. Era bravo in quello che faceva e gli piaceva. Una presa di coscienza molto lunga e combattuta, ma che ci rivela la vera natura del personaggio, natura che per quanto sopita e latente è presente in _Walt_ fin dalla prima inquadratura. In questo spin-off la situazione si capovolge; James McGill, e tutto il pubblico, conosce bene la sua natura da truffatore, ma cerca con tutte le sue forze(e quelle di suo fratello) di lasciarsi alle spalle _Slippin Jimmy_ per diventare un grande avocato. Lo spettatore per tutta la prima stagione accompagna Jimmy per una via tortuosa e piena di ostacoli, costellata da tante sconfitte, ma anche qualche soddisfazione. Ormai ce l'aveva fatta, Jimmy era pronto a lavorare per un grande studio legale e noi eravamo pronti a gioire per lui, ma è proprio in questo momento capisce che quella vinta non fa per lui, capisce che diventare avocato non era il suo sogno, lui "da grande" vuole fare il truffatore. Tutte le frasi fatte che ci accompagnano fin dalla nascita vanno a farsi benedire. "Si te stesso e andrà tutto bene", "fa quello che ti piace e sarai felice", tutto questo crolla e perde di significato, la teoria lascia spazio alla pratica e Jimmy cede il posto a Saul.

[1]: https://it.wikipedia.org/wiki/Ferro
[2]: https://it.wikipedia.org/wiki/Litio
[3]: https://it.wikipedia.org/wiki/Sodio
