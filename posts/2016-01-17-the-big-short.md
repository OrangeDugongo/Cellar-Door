---
title: The Big Short
author: Raffaele
date: 2016-01-17
categories:
    - cinema
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*vsA6M8s_gGIkpGq1bVzGJw.jpeg
permalink: /cinema/la-grande-scommessa/
---

Il&nbsp;Cinema non è intrattenimento. O almeno per me il Cinema non può e non deve essere solo intrattenimento, il Cinema è qualcosa di più profondo. I film devono cambiare lo spettatore, devono insegnarli qualcosa di nuovo, devono innescare pensieri e riflessioni, per me il Cinema è questo e _La Grande Scommessa_ è Cinema.  
La commedia di McKay è un divertente, quanto cinico affresco della Wall Street, ma degli Stati Uniti in generale, pre-crisi. La vicenda segue tre gruppi d'investitori, geniali e sconosciuti all'alta finanza, che si accorgono prima di chiunque altro del terribile disastro economico in cui si stava per imbattere il mondo. Il linguaggio è molto tecnico e a primo impatto stordirà gli spettatori a digiuno di economia, ma col passare del tempo tutti questi tecnicismi verranno spiegati con parole ed esempi semplici e chiari. Quasi per dire: "Non è l'economia ad essere difficile, ma sono le banche a renderla difficile". Perché l'intento, per niente velato, di questo film è denunciare le schifezze e le frodi operate dalle banche.

Lo&nbsp;stile del film è molto documentaristico. La regia _"camera in mano"_ricorda molto lo stile di [Moore][1], caratterizzata da continui zoom sui volti dei protagonisti. Gli attori sono consapevoli di essere ripresi, spesso e volentieri bucano la quarta parete per parlare direttamente con lo spettatore che diventa il vero protagonista della commedia. Il montaggio è frenetico, incalzante, le due ore e venti sono dense d'eventi, lo spettatore non tempo per annoiarsi. Tutti questi elementi sono tenuti insieme da una sceneggiatura equilibrata e ben fatta. Una vittoria agli oscar in questa categoria non mi sorprenderebbe affatto.  
Ottimo anche il lavoro attoriale, tra tutti Christian Bale che ci regala una delle sue migliori interpretazioni, ma soprattutto Steve Carell che dimostra ancora una volta di avere un grande talento sia comico che drammatico. Personalmente tra i due avrei preferito una nomination per Carell, ma va bene anche così.  
Unica nota dolente è il trailer un po' ingannevole, ti presenta un film diverso da com'è in realtà.

La&nbsp;Crisi, sono anni che ne sentiamo parlare, ma fondamentalmente non ci abbiamo capito un c***o. Alzi la mano chi di voi sarebbe in grado di spiegarne le cause a mia nonna. Nessuno? Bene, allora correte al cinema, perché McKay, con la sua ultima commedia, ci riesce e anche bene.

## Spoiler

### Da qui in poi ci saranno degli spoiler, se non hai ancora visto il film ti consiglio di non continuare.

Quello che ho adorato di più in questo film è il finale amaro. Tutti i nostri protagonisti hanno fatto un mare di soldi eppure non saltano di gioia, non ci sono fuochi d’artificio, non ci sono vincitori, ma solo vinti.
Il personaggio di Bale ha quadruplicato il suo denaro, ma ha lasciato lungo la strada i suoi colleghi, i suoi clienti e le sue certezze. I due ragazzi hanno scommesso contro il loro paese, la loro gente e le loro famiglie e hanno vinto. Il personaggio di Carell tanto desideroso di combattere il sistema è diventato esso stesso il sistema. Queste sono le commedie che mi piacciono, questo è il Cinema che mi piace.

[1]: http://www.cellar-door.xyz/cinema/bowling-for-columbine/
