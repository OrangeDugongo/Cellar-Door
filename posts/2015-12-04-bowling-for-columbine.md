---
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*79tuXqDGoEu8zoN777Nkpw.jpeg
title:  "Bowling for Columbine"
date:   2015-12-04
author: Raffaele
categories:
    - cinema
permalink: /cinema/bowling-for-columbine/
---

Come probabilmente avrai già capito dal titolo oggi parliamo di Bowling for Columbine, un documentario di Michael Moore vincitore anche del premio Oscar, quindi roba di qualità e non l'ennesimo docufilm sulla vita di Steve Jobs. Il documentario parte dal massacro della Columbine High School per capirne le cause e impedire il ripetersi di episodi di violenza simile. Nonostante il documentario sia del 2002 le tematiche purtroppo restano tuttora attuali e irrisolte. Michael Moore, come suo solito, presenta la vicenda molto soggettivamente e porta avanti la sua tesi senza considerare altri punti di vita. Lo so, ad alcuni fa storcere il naso e capisco anche che può non piacere, ma io apprezzo molto questo modo di fare, si vede che Moore crede in quello che sta facendo e da tutto se stesso per avvicinare lo spettatore alla vicenda. Questa sua passione rende il tutto molto più vivo e reale.

Quello che più mi ha sconvolto non è tanto la facilità con cui un americano può entrare in possesso di armi e munizioni, ma il clima di terrore che respirano. Media e politici fanno di tutto per spaventare la popolazione, più li terrorizzano più sono felici, ma la felicità conta poco; i soldi, quelli si che contano. Un cittadino impaurito compra sistemi di sicurezza e armi; poi se la stessa arma viene usata per fare una strage al centro disabili, non importa, sono cose che capitano.  
