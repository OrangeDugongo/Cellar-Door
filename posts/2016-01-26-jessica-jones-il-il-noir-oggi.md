---
title: Jessica Jones, il noir oggi
author: Raffaele
date: 2016-01-26
categories:
- serie-tv
- netflix
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*N91UtBdrFjuYS5xFvWa1lw.jpeg
permalink: /serie-tv/jessica-jones-noir-oggi/
---

Inutile girarci in torno, Jessica Jones era una delle serie più attese dell'ultimo periodo. Dopo l'incredibile successo riscosso da [_Daredevil][1]&nbsp;_l'hype verso Jessica è cresciuto a dismisura. Proprio per questo Netflix ha cercato di riproporre la stessa ricetta, ma con qualche piccola variazione. Se in Devil si respirava l'aria di Hell's Kitchen e l'intera serie aveva un'impalcatura corale, questa seconda serie Marvel assume una conformazione molto più personale e intima. La lotta contro i demoni riaffiorati dal passato coinvolgeranno in prima persona lo spettatore che si trova a vivere le stesse ansie, le stesse paure della protagonista. Quindi ci troviamo di nuovo difronte ad un eroe tormentato che di eroico ha veramente poco. Jessica, diversamente da Matt, non ci prova neanche a fare la eroina, piuttosto utilizza i suoi poteri come un arma in più per svolgere al meglio il suo lavoro d'investigatrice. Anche il genere è diverso, non più in thriller dai toni dark, ma un noir psicologico che punta molto sull'investigazione, o almeno lo fa nelle prime puntate. Quello che si nota guardando Jessica Jones è proprio il calo tra i primi episodi e il resto della stagione. La trama si sviluppa in modo originale e perfetto per i primi sei episodi, si raggiunge un punto di svolta, si potrebbe chiudere lì, invece no. Con un plot twist stra-riciclato si decide di tirarla per le lunghe, riproponendo più volte le stesse situazioni fino ad annoiare lo spettatore. Nelle ultime puntate non viene aggiunto niente di nuovo alla trama, vango solo aggiunti(buttati un po lì) tanti piccoli appigli su cui sviluppare un universo televisivo condiviso. Viene spianata la strada alla prossima serie su Luke Cage, si apre una piccola porta a Hellcat e a Nuke, si inizia a costruire qualche ponte con Daredevil, ma sopratutto si gettano le basi per una seconda stagione che difficilmente vedrà la luce(Okay Netflix mi ha smentito, Jessica Jones è stata rinnovata per una seconda stagione).

Caro lettore, come avrai già capito l'unico punto debole di questa serie è una scrittura un po' grossolana e frettolosa. Per il resto siamo sui livelli di Daredevil. La fotografia convince e la regia è s_empre sul pezzo. _Anche le performans attoriali non sono niente male. Krysten Ritter è perfetta nei panni di Jessica Jones, ha proprio la faccia da stronzetta(con affetto naturalmente) e riesce a portare sullo schermo una donna forte che mancava da tanto nelle serie TV. Ma la vera star dello show è David Tennant, anche se il suo personaggio, Killgrave, incute più terrore quando non è fisicamente sulla scena. La colpa non è sua, ma degli autori, incapaci di sfruttare a pieno il suo talento.

Concludendo è impossibile nascondere la mia delusione, non che Jessica sia un pessimo prodotto, ma gli standard rispetto a qualche anno fa si sono alzati e di molto, ma nonostante tutto sono fiducioso per il futuro. Le fondamenta ci sono e sono fatte bene, Jessica con i suoi problemi e lo stampo fortemente femminista ha potenzialità narrative immense, si deve soltanto aggiustare un po' il tiro.

[1]: /serie-tv/daredevil-the-dark-side-of-the-marvel/
