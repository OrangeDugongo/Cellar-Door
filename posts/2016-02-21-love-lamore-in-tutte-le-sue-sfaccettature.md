---
title: Love, l'amore in tutte le sue sfaccettature
permalink: /serie-tv/love/
author: Raffaele
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*7f-aZK9bpRpCtP8JthUsoA.jpeg
date: 2016-02-21
categories:
    - serie-tv
    - netflix
---

Lacommedia romantica è uno dei generi più abusati dal cinema e dalla televisione; negli ultimi anni siamo stati tempestati da una valanga di produzione tutte uguali tra loro. Ancora prima di iniziare la visone lo spettatore ha gia ben chiaro come andrà a finire. In un panorama dove è la mediocrità a farla da protagonista distinguersi è diventato sempre più difficile, ma per fortuna le eccezioni esistono ancora. L’incipit di ***LOVE*** è quanto di più banale si possa immaginare. Gus, un simpatico *nerd* e Mickey, una non più giovanissima ribelle, reduci da due relazioni fallimentari, sonocostretti ad incontrarsi dal destino. Mio lettore, probabilmente ti sarà già passata la voglia di vedere anche solo il pilot, ma ti posso assicurare che in questa serie non conta tanto ne l’inizio ne la fine, gli autori hanno preferito piuttosto concentrarsi nel mezzo, sul percorso intrapreso dai due protagonisti. Poi aggiungici le ottime performance di Paul Rust, qui anche in veste di autore, e della stupenda Gillian Jacobs e la qualità è servita.

Quello che da *buon nerd* ho apprezzato di più di questa serie è la spiccata vena metanarrativa. Gli autori, grazie al lavoro del protagonista, ci introducono sul set di una produzione televisiva dove citazioni e riferimenti ad altri show, passati e presenti, si sprecano. Non c’è solo il citazionismo, ma anche critica ad un mondo dello spettacolo troppo orientato agli ascolti e ai profitti tanto da mettere in secondo piano i contenuti, la qualità e le persone che contribuisco a creare questi prodotti(in questo mi ha ricordato molto un [altro show][bojack] Netflix). Judd, però, non si accontenta del solo mondo audiovisivo e preferisce andare oltre creando un suggestivo ritratto-specchio dalla cultura pop dell’ultimo ventennio.
Un altro tema fondamentale nell’economia del racconto sono i rapporti di amicizia che si instaurano non solo tra i due protagonisti, ma anche tra i comprimari. Gli amici che sono presenti nel momento del bisogno, che ci rincuorano quando siamo tristi e ci strigliano quando ce lo meritiamo. Gli amici che a volte ci riempiono di consigli non richiesti e altre volte ci ignorano, ma che, in fin dei conti, sono sempre lì quando servono.

*Ok, ma l’amore in tutto questo dov’è?*

L’amore è sempre presente, funge da vera e propria spina dorsale della serie. L’amore ci viene presentato in tutte le sue sfaccettature; l’amore come bugia, la menzogna del vissero felici e contenti che ci viene raccontata dai film e dalle favole. L’amore come attrazione, come un semplice atto di piacere che alla lungo può trasformarsi in una vera e propria dipendenza affettiva. E infine l’amore come Amore, quello capace di tutto, anche di salvarci da un coma glicemico dopo una giornata di merda.

**LOVE** è il raccondo dei trentenni mai cresciuti, blocati in una tarda, quanto estrema giovinezza costretta a fare i conti con *il mondo degli adulti*, con il lavoro e con la ricerca di una relazione che vada oltre la semplice attrazione sessuale.

[bojack]: /serie-tv/bojack-horseman-storia-un-rinunciatario/
