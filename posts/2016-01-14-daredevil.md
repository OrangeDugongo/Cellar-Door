---
title: The Dark Side of The Marvel
author: Raffaele
date: 2016-01-14
categories:
    - serie-tv
    - netflix
layout: header-image-post
headerimg: https://cdn-images-1.medium.com/max/800/1*5S7UXRjc2bPJeXNhTOTpMw.jpeg
permalink: /serie-tv/daredevil-the-dark-side-of-the-marvel/
---

_Daredevil_ è il primo frutto della collaborazione tra Netflix e Marvel; un incontro tutt'altro che facile tra due realtà creative che si rivolgono ad un pubblico diverso. Marvel, quindi Disney, con i suoi cinecomics ci ha abituati ad eroi colorati, ad un'atmosfera giocosa ed a un tono sempre orientato verso la commedia. Risulta evidente che tutte queste caratteristiche mal si adattano alle serie Netflix(tutte vietate ai minori di 14 anni), proprio per questo sono stati scelti i personaggi più tormentati che l'universo Marvel potesse offrire e il risultato è sorprendente.

Grande attenzione nel corso della serie viene data alla morale, man mano che passano le puntate distinguere chi è il buono e chi il cattivo diventa sempre più difficile, gli stessi personaggi non sanno realmente chi sono. In questo senso ho trovato particolarmente azzeccati i vari rimandi al cattolicesimo e alla Bibbia. Matt e Fisk(interpretato da un sorprendente Vincent D'Onofrio) sono due vittime di Hell's Kitchen e non posso accettare che in futuro si ripeta quanto successo loro. Entrambi sono pronti a lottare per dare un futuro migliore alla città, ma sceglieranno strade completamente diverse che li porterà a fare scelte moralmente ambigue, ma necessarie per un bene più alto. Tutta la prima stagione ruota intorno all'importanza della scelta. Matt non è ancora Devil, ma solo un giustiziere mascherato, così come Fisk ancora non diventa Kingpin, ma grazie alle decisioni prese in queste tredici puntate capiscono(e capiamo) chi sono e soprattutto cosa vorranno essere. Tutto questo non solo crea personaggi a tutto tondo ma li rende anche dinamici e vivi cosa che porta lo spettatore ad affezionarsi sempre di più. Affetto che non si limita ai soli protagonisti, ma si estende anche ai personaggi secondari, tutti caratterizzati in modo eccelso ed ognuno con la propria story line.

Dal punto di vista tecnico la serie TV rispetta appieno gli standard qualitativi Netflix, ogni dettaglio è curato alla perfezione, non c'è niente fuori posto. La fotografia, caratterizzata da colori acidi confinati tra il giallo e il viola, è quanto di meglio possa offrire oggi la televisione. La reggia è ottima e mai banale, memorabile il piano sequenza ripreso da _Old Boy_ o l'esplosione dei palazzi che ricorda molto il finale di _Fight Club_. Anche la scelta della colonna sonora è scelta con cura(una delle scene più belle si svolge sulle note di _Nessun Dorma_). Ma la ciliegina sulla torta è la scrittura, capace di prendersi il suo tempo rendendo rapporti e dinamiche tra i vari personaggi il quanto più simili alla vita reale. Matt dopo una scazzottata ci mette del tempo per riprendersi, così come non basta una puntata per tornare amici e ci vuole del tempo per fare pace con il proprio senso di colpa.

Potrei andare avanti a lungo sprecando parole non necessarie visto che per descrivere _Daredevil_ ne bastano veramente poche; la migliore serie TV su un supereroe.  
